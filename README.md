# NOTE: 07/22/16: Please use Docker instead of Local Installation Instructions

# Local Installation Instructions using R 3.2 
    source("https://bioconductor.org/biocLite.R")
    biocLite("rcellminer")
    biocLite("rcellminerData")

# Install Dependencies 
    if (!"devtools" %in% installed.packages()) install.packages("devtools")
    
    setRepositories(ind=1:6)
    library(devtools)

    install_bitbucket("cbio_mskcc/rcellminerelasticnet",
      auth_user="discoverUser",
      password="discoverUserPassword",
      build_vignettes=FALSE,
      dependencies=TRUE,
      args="--no-multiarch")
    
    # Custom pheatmap version  
    install_github("cannin/pheatmap", dependencies=TRUE)

# Docker Instructions 

## Download Data

Elastic Net results and resulting data is located at this URL: http://sanderlab.org/swathAnalysis/swathOutput_062316_all.tar.gz

## Download SWATH Docker container
    docker pull cannin/swath

## Run Docker

Set data and output directories as necessary in the Docker command:

* SWATH_OUTPUT_DIR to wherever the swathOutput tar.gz file was unzipped
* OUTPUT_DIR to a directory where new results can be outputted (may be same as SWATH_OUTPUT_DIR)
 
Docker command  
 
    docker run --name swath -v SWATH_OUTPUT_DIR:/home/rstudio/data -v OUTPUT_DIR:/home/rstudio/output -p 3838:3838 -p 8787:8787 -t cannin/swath

### Example 

    docker run --name swath -v ~/Downloads/swathOutputData:/home/rstudio/data -v ~/Downloads/output:/home/rstudio/output -p 3838:3838 -p 8787:8787 -t cannin/swath

## rcellminer Shiny App
The rcellminer R Shiny app runs automatically within the container on port 3838 (NOTE: Check that you are using the correct IP address for your Docker installation)

URL: 127.0.0.1:3838

## Access RStudio Server
RStudio Server runs automatically within the container on port 8787 (NOTE: Check that you are using the correct IP address for your Docker installation)

URL: 127.0.0.1:8787 

### RStudio Server (Username/Password)

    rstudio/rstudio

## Docker Quickstart 

### Open Project

Open Rproj file in RStudio in /workspace/rcellminerElasticNet

### Run Elastic Net analysis 

Open a knitr (.Rmd) of interest to be re-run from /home/rstudio/data. Change the following line to /home/rstudio/output

    outputDir <- "/data/augustin/swathOutput_062316"