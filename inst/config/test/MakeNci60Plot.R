#--------------------------------------------------------------------------------------------------
# SET PARAMETERS
#--------------------------------------------------------------------------------------------------
# Data types:
# act: drug activity
# exp: mRNA expression (z-score)
# xai: mRNA expression (log2)
# swa: SWATH-MS protein expression
# mut: DNA mutation (% conversion to variant)

yAxisDataType <- "swa"
yAxisFeatureName <- "LAMTOR3"
yAxisFeatureLabel <- "LAMTOR3" 

xAxisDataType <- "act"
xAxisFeatureName <- "761431"
xAxisFeatureLabel <- "Vemurafenib"

plotLegendLocation <- "bottomright" # "bottomleft"

plotFileName <- paste0(yAxisDataType, yAxisFeatureLabel, "_vs_",
                       xAxisDataType, xAxisFeatureLabel, ".pdf")

# Note: for drugs, please use NSC identifiers (as character vectors, not integers), e.g.,
# yAxisDataType <- "act"
# yAxisFeatureName <- "609699"      # data matrix rowname identifier
# yAxisFeatureLabel <- "Topotecan"  # plot label
#--------------------------------------------------------------------------------------------------
# LOAD DATA
#--------------------------------------------------------------------------------------------------
library(rcellminer)

drugAct <- exprs(rcellminer::getAct(rcellminerData::drugData))
molDB   <- getAllFeatureData(rcellminerData::molData)

nci60ColorTab <- rcellminer::loadNciColorSet(returnDf = TRUE)
stopifnot(identical(nci60ColorTab$abbrCellLines, colnames(drugAct)))
tissueColorTab <- unique(nci60ColorTab[, c("tissues", "colors")])
tissueColorTab$tissues <- toupper(tissueColorTab$tissues)
tissueColorTab[2, "tissues"] <- "CNS"

getNci60Data <- function(dataType, featureName){
  if (dataType == "act"){
    nci60Data <- setNames(drugAct[featureName, , drop=TRUE],
                          colnames(drugAct))
  } else{
    nci60Data <- setNames(molDB[[dataType]][featureName, , drop=TRUE],
                          colnames(molDB[[dataType]]))
  }
  return(nci60Data)
}

yData <- getNci60Data(yAxisDataType, yAxisFeatureName)
xData <- getNci60Data(xAxisDataType, xAxisFeatureName)
#--------------------------------------------------------------------------------------------------
# MAKE PLOT, COMPUTE PEARSON'S CORRELATION
#--------------------------------------------------------------------------------------------------
pdf(file = plotFileName, width = 8, height = 8)
plot(x=xData, y=yData, col=nci60ColorTab$colors, pch=16,
     xlab=xAxisFeatureLabel, ylab=yAxisFeatureLabel)
abline(fit <- lm(yData ~ xData), col="red")
legend("topleft",bty="n",
       legend=bquote(R^2 == .(format(summary(fit)$adj.r.squared, digits = 4))))
legend(plotLegendLocation, legend=tissueColorTab$tissues, col=tissueColorTab$colors, 
       cex=0.7, pch = 16)
dev.off()

cor.test(xData, yData)
#--------------------------------------------------------------------------------------------------

